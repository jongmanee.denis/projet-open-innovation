<?php

class EventManager extends Manager {
	
	public function listFriendsEvents(String $email) {
		$conn = $this -> connectDB();
		$sql = "SELECT id_event, u1.lastName_user, u1.firstName_user, title_event FROM Event INNER JOIN User u1 ON Event.email_user=u1.email_user INNER JOIN Friend ON u1.email_user=Friend.email_friend INNER JOIN User u2 ON Friend.email_user=u2.email_user WHERE u2.email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			return $result;
		}
	}

	public function listEventParticip($email) {
		$conn = $this -> connectDB();
		$sql = "SELECT id_event, title_event, date_event, number_places, num_sport FROM Event INNER JOIN Participant ON Event.id_event=Participant.num_event INNER JOIN User ON Participant.email_user=User.email_user WHERE Participant.email_user=? AND datediff(now(),date_event)<0 ORDER BY date_event;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$events = $result-> fetchAll(PDO::FETCH_CLASS, "Event");
			return $events;
		}
	}

	public function getSomeEvents($email) {
		$conn = $this -> connectDB();
		$sql = " SELECT Event.* FROM follow INNER JOIN Sport ON follow.num_sport=sport.id_sport INNER JOIN event ON sport.id_sport=event.num_sport WHERE follow.email_user=? AND datediff(now(), event.date_event)<0 EXCEPT SELECT Event.* FROM participant INNER JOIN Event ON participant.num_event=event.id_event WHERE participant.email_user=?";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email, $email));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$events = $result-> fetchAll(PDO::FETCH_CLASS, "Event");
			return $events;
		}
	}

	public function getCreatedEventsUser($email) {
		$conn = $this -> connectDB();
		$sql = "SELECT * FROM Event WHERE email_user=? ORDER BY date_event DESC";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$events = $result-> fetchAll(PDO::FETCH_CLASS, "Event");
			return $events;
		}
	}

	public function listAllEventParticip($email) {
		$conn = $this -> connectDB();
		$sql = "SELECT id_event, title_event, date_event, number_places, num_sport FROM Event INNER JOIN Participant ON Event.id_event=Participant.num_event INNER JOIN User ON Participant.email_user=User.email_user WHERE Participant.email_user=? ORDER BY date_event;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$events = $result-> fetchAll(PDO::FETCH_CLASS, "Event");
			return $events;
		}	
	}
 
	public function addEvent($event) {
		$conn = $this -> connectDB();
		$sql = "INSERT INTO Event (title_event, description_event, date_event, duration_event, number_places, num_sport, email_user) VALUES (?,?,?,?,?,?,?);";
		$result = $conn -> prepare($sql);
		var_dump($event);
		$result -> execute(array($event->getTitle_event(), $event->getDescription_event(), $event->getDate_event(), $event->getDuration_event(), $event->getNumber_places(), $event->getNum_sport(), $event->getEmail_user()));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$message = "Vous avez créé un évenement";
			return $message;
		}
	}

	public function getEventByIdSport($id) {
		$conn = $this -> connectDB();
		$sql = "SELECT * FROM Event WHERE num_sport=?";
		$result = $conn -> prepare($sql);
		$result -> execute(array($id));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$events = $result-> fetchAll(PDO::FETCH_CLASS, "Event");
			return $events;
		}
	}

	public function getEvent($id) {
		$conn = $this -> connectDB();
		$sql = "SELECT * FROM Event WHERE id_event=?";
		$result = $conn -> prepare($sql);
		$result -> execute(array($id));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$event = $result-> fetchAll(PDO::FETCH_CLASS, "Event");
			return $event[0];
		}
	}

	public function getEventsByUser($email) {
		$conn = $this -> connectDB();
		$sql = "SELECT * FROM Event WHERE email_user=?";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) { 
			die("Echec de l'exécution de la requête.<br />"); 
		} else {
			$events = $result-> fetchAll(PDO::FETCH_CLASS, "Event");
			return $events;
		}
	}
}