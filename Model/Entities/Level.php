<?php

class Level {
	
	private int $id_level;
	private String $wording_level;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

	    switch($ctp) {
	    	case 1:
	    		$this->id_level = isset($args[0]['id_level']) ? $args[0]['id_level'] : -1;
	    		$this->wording_level = isset($args[0]['wording_level']) ? $args[0]['wording_level'] : "";
	    		break;
	    	default:
	    		break;
	    }
	}

	public function getId_level() : int {
		return $this->id_level;
	}

	public function getWording_level() : String {
		return $this->wording_level;
	}
}