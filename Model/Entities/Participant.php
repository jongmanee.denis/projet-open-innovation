<?php

class Participant {
	
	private String $email_user;
	private int $num_event;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

	    switch($ctp) {
	    	case 1:
	    		$this->email_user = isset($args[0]['email_user']) ? $args[0]['email_user'] : "";
	    		$this->num_event = isset($args[0]['num_event']) ? $args[0]['num_event'] : -1;
	    		break;	
	    	default:
	    		break;    	
		}
	}

	public function getEmail_user() : String {
		return $this->email_user;
	}

	public function getNum_event() : int {
		return $this->num_event;
	}
	
}