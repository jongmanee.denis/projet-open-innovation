<?php

class Friend {
	
	private String $email_user;
	private String $email_friend;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

     	switch($ctp) {
    	
	    	case 1:
	            $this->email_user = isset($args[0]['email_user']) ? $args[0]['email_user'] : "";
	            $this->email_friend = isset($args[0]['email_friend']) ? $args[0]['email_friend'] : "";
	    		break;

			default:
				break;
		}
	}

	public function getEmail_user() : String {
		return $this->email_user;
	}

	public function getEmail_friend() : String {
		return $this->email_friend;
	}


}