<?php

class Event {

	private int $id_event;
	private String $title_event;
	private String $description_event;
	private String $date_event;
	private String $duration_event;
	private int $number_places;
	private int $num_sport;
	private String $email_user;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

     	switch($ctp) {
	 	
	    	case 1:
	    	var_dump($args);
	    		$this->id_event = isset($args[0]['id_event']) ? $args[0]['id_event'] : -1;
	    		$this->title_event = isset($args[0]['title_event']) ? $args[0]['title_event'] : "";
	    		$this->description_event = isset($args[0]['description_event']) ? $args[0]['description_event'] : "";
	    		$this->date_event = isset($args[0]['date_event']) ? $args[0]['date_event'] : "";
	    		$this->duration_event = isset($args[0]['duration_event']) ? $args[0]['duration_event'] : -1;
	    		$this->number_places = isset($args[0]['number_places']) ? $args[0]['number_places'] : -1;
	    		$this->num_sport = isset($args[0]['num_sport']) ? $args[0]['num_sport'] : -1;
	    		$this->email_user = isset($args[0]['email_user']) ? $args[0]['email_user'] : "";
	    		break;
	 
	    	default:
	    		break;
		}
	}

	public function getId_event() : int {
		return $this->id_event;
	}

	public function getTitle_event() : String {
		return $this->title_event;
	}

	public function getDescription_event() : String {
		return $this->description_event;
	}

	public function getDate_event() : String {
		return $this->date_event;
	}

	public function getDuration_event() : String {
		return $this->duration_event;
	}

	public function getNumber_places() : int {
		return $this->number_places;
	}

	public function getNum_sport() : int{
		return $this->num_sport;
	}

	public function getEmail_user() : String {
		return $this->email_user;
	}

}