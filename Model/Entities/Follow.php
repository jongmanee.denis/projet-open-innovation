<?php 

class Follow {

	private String $email_user;
	private int $num_sport;

	public function __construct() {
		
	    $ctp = func_num_args();
	    $args = func_get_args();

	    switch($ctp) {

	    	case 1:
	    		$this->email_user = isset($args[0]['email_user']) ? $args[0]['email_user'] : "";
	    		$this->num_sport = isset($args[0]['num_sport']) ? $args[0]['num_sport'] : -1;
	    		break;
	    	default:
	    		break;
	    }
	}

	public function getEmail_user() : String {
		return $this->email_user;
	}

	public function getNum_sport() : int {
		return $this->num_sport;
	}
}