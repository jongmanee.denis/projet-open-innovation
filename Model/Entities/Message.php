<?php

class Message {

	private String $date_message;
	private String $text_message;
	private String $email_sender_user;
	private String $email_receiver_user;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

	    switch($ctp) {
	    	case 1:
	    		$this->date_message = isset($args[0]['date_message']) ? $args[0]['date_message'] : date("Y-m-d H:i:s");
	    		$this->text_message = isset($args[0]['text_message']) ? $args[0]['text_message'] : "";
	    		$this->email_sender_user = isset($args[0]['email_sender_user']) ? $args[0]['email_sender_user'] : "";
	    		$this->email_receiver_user = isset($args[0]['email_receiver_user']) ? $args[0]['email_receiver_user'] : "";
	    		break;
	    	default:
	    		break;
	    }
	}

	public function getDate_message() : String {
		return $this->date_message;
	}

	public function getText_message() : String {
		return $this->text_message;
	}

	public function getEmail_sender_user() : String {
		return $this->email_sender_user;
	}

	public function getEmail_receiver_user() : String {
		return $this->email_receiver_user;
	}
}