<?php

class Post {

	private String $date_post;
	private String $email_user;
	private String $text_post;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

	    switch($ctp) {
	    	case 1:
	    		$this->date_post = isset($args[0]['date_post']) ? $args[0]['date_post'] : date("Y-m-d H:i:s");
	    		$this->email_user = isset($args[0]['email_user']) ? $args[0]['email_user'] : "";
	    		$this->text_post = isset($args[0]['text_post']) ? $args[0]['text_post'] : "";
	    		break;	
	    	default:
	    		break;    	
		}
	}

	public function getDate_post() : String {
		return $this->date_post;
	}

	public function getEmail_user() : String {
		return $this->email_user;
	}

	public function getText_post() : String {
		return $this->text_post;
	}
}