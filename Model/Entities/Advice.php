<?php

class Advice {
	private int $id_advice;
	private String $text_advice;
	private int $num_level;
	private int $num_sport;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

	    switch($ctp) {
	    	case 1:
	    		$this->id_advice = isset($args[0]['id_advice']) ? $args[0]['id_advice'] : -1;
	    		$this->text_advice = isset($args[0]['text_advice']) ? $args[0]['text_advice'] : "";
	    		$this->num_level = isset($args[0]['num_level']) ? $args[0]['num_level'] : -1;
	    		$this->num_sport = isset($args[0]['num_sport']) ? $args[0]['num_sport'] : -1;
	    		break;
	    	default:
	    		break;
	    }
	}

	public function getId_advice() : int {
		return $this->id_advice;
	}

	public function getText_advice() : String {
		return $this->text_advice;
	}

	public function getNum_level() : int {
		return $this->num_level;
	}

	public function getNum_sport() : int {
		return $this->num_sport;
	}
}