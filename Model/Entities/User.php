<?php

class User {
	
	private String $pseudo_user;
	private String $lastName_user;
	private String $firstName_user;
	private String $email_user;
	private String $password_user;
	private String $gender_user;
	private String $birthday_user;
	private String $cpassword_user;

	public function __construct() {

	    $ctp = func_num_args();
	    $args = func_get_args();

	    switch($ctp) {
	    	
	    	case 1:
				$this->pseudo_user = isset($args[0]['pseudo_user']) ? $args[0]['pseudo_user'] : "";
				$this->lastName_user = isset($args[0]['lastName_user']) ? $args[0]['lastName_user'] : "";
				$this->firstName_user = isset($args[0]['firstName_user']) ? $args[0]['firstName_user'] : "";
				$this->email_user = isset($args[0]['email_user']) ? $args[0]['email_user'] : "";
				$this->password_user = isset($args[0]['password_user']) ? $args[0]['password_user'] : "";
				$this->gender_user = isset($args[0]['gender_user']) ? $args[0]['gender_user'] : "";
				$this->birthday_user = isset($args[0]['birthday_user']) ? $args[0]['birthday_user'] : "";
				$this->cpassword_user = isset($args[0]['cpassword_user']) ? $args[0]['cpassword_user'] : "";

	    		break;
	    	
	    	default:
	    		break;
    	}
    }

    public function getId_user() : int {
    	return $this->id_user;
    }

    public function getPseudo_user() : String {
    	return $this->pseudo_user;
    }

    public function getFirstName_user() : String {
    	return $this->firstName_user;
    }
	
	public function getLastName_user() : String {
		return $this->lastName_user;
	}

	public function getEmail_user() : String {
		return $this->email_user;
	}

	public function getPassword_user() : String {
		return $this->password_user;
	}

	public function getGender_user() : String {
		return $this->gender_user;
	}

	public function getBirthday_user() : String {
		return $this->birthday_user;
	}

		public function getCpassword_user() : String {
		return $this->cpassword_user;
	}
}