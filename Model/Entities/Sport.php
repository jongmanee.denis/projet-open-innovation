<?php

class Sport {
	
	private int $id_sport;
	private String $name_sport;
	private String $description_sport;

	public function __construct() {
		
		$ctp = func_num_args();
    	$args = func_get_args();

		switch($ctp) {
			case 1:
				$this->id_sport = isset($args[0]['id_sport']) ? $args[0]['id_sport'] : -1;
				$this->name_sport = isset($args[0]['name_sport']) ? $args[0]['name_sport'] : "";
				$this->description_sport = isset($args[0]['description_sport']) ? $args[0]['description_sport'] : "";
				break;
			default:
				break;
		}
	}

	public function getId_sport() : int {
		return $this->id_sport;
	}

	public function getName_sport() : String {
		return $this->name_sport;
	}

	public function getDescription_sport() : String {
		return $this->description_sport;
	}
}