<?php


class SportManager extends Manager {

	public function getName_sportbyId($id) {
		$conn = $this->connectDB();
		$sql = "SELECT name_sport FROM Sport WHERE id_sport=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($id));
		if ($result == False) {
			die('Problème de la requête sql');
		} else {	
			$name_sport = $result ->fetch();
			return $name_sport[0];
		}
	}
	
	public function getOthersSports($email) {
		$conn = $this->connectDB();
		$sql = "SELECT Sport.* FROM sport EXCEPT SELECT Sport.* FROM Sport INNER JOIN Follow ON Sport.id_sport=Follow.num_sport where Follow.email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) {
			die('Problème de la requête sql');
		} else {
			$sports = $result->fetchAll(PDO::FETCH_CLASS, 'Sport');
			return $sports;
		}

	}

	public function getSports() {
		$conn = $this->connectDB();
		$sql = "SELECT * FROM Sport";
		$result = $conn -> query($sql);
		if ($result == False) {
			die('Problème de la requête sql');
		} else {
			$sports = $result->fetchAll(PDO::FETCH_CLASS, 'Sport');
			return $sports;
		}
	}
}