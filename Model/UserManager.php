<?php

class UserManager extends Manager {
	
	public function login(User $user) {
		$conn = $this -> connectDB();
		$sql = "SELECT email_user FROM User WHERE email_user=? AND password_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($user->getEmail_user(), $user->getPassword_user()));
		if ($result == False) {
			die("Probleme d'exécution de la requête SQL");
		} else {
			$email = $result -> fetch();
			if ($email == False) {
				return False;
			} else {
				return $email[0];
			}
		}
	}

	public function uniqueEmail(User $user) : Bool {
		$conn = $this -> connectDB();
		$sql = "SELECT email_user FROM USER WHERE email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($user->getEmail_user()));
		if ($result == False) {
			die("Probleme d'exécution de la requête SQL");
		} else {
			$email = $result -> fetch();
			if ($email == False) {
				return True;
			} else {
				return False;
			}
		}
	}

	public function registration(User $user) : String {
		$conn = $this -> connectDB();
		$sql = "INSERT INTO User (email_user, password_user, lastName_user, firstName_user, birthday_user, gender_user, pseudo_user) VALUES (?,?,?,?,?,?,?);";
		$result = $conn -> prepare($sql);
		$result -> execute(array($user->getEmail_user(), $user->getPassword_user(), $user->getLastName_user(), $user->getFirstName_user(), $user->getBirthday_user(), $user->getGender_user(), $user->getPseudo_user()));
		if ($result == False) { 
			die("Probleme d'exécution de la requête SQL");
 		} else {
 			$message = "Inscription réussie !";		
 		}
		return $message;
	}



	public function getUser($email) {
		$conn = $this -> connectDB();
		$sql = "SELECT * FROM User WHERE email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) {
			die("Probleme d'exécution de la requête SQL");
		} else {
			$user = $result -> fetchAll(PDO::FETCH_CLASS, "User");
			
			return $user[0];
		}
	}

	public function getFirstName($email) : String {
		$conn = $this -> connectDB();
		$sql = "SELECT firstName_user FROM User WHERE email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) {
			die("Probleme d'exécution de la requête SQL");
		} else {
			$firstName = $result -> fetch();
			if ($firstName == False) {
				return False;
			} else {
				return $firstName[0];
			}
		}
	}

	public function getLastName($email) : String {
		$conn = $this -> connectDB();
		$sql = "SELECT lastName_user FROM User WHERE email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) {
			die("Probleme d'exécution de la requête SQL");
		} else {
			$lastName = $result -> fetch();
			if ($lastName == False) {
				return False;
			} else {
				return $lastName[0];
			}
		}	
	}


	public function updateProfile(User $user) : String {
		$conn = $this ->connectDB();
		$sql = "UPDATE User SET lastName_user=?, firstName_user=?, birthday_user=?, gender_user=?, pseudo_user=? WHERE email_user=?";
		$result = $conn -> prepare($sql);
		$result -> execute(array($user->getLastName_user(), $user->getFirstName_user(), $user->getBirthday_user(), $user->getGender_user(), $user->getPseudo_user(), $user->getEmail_user()));
		if ($result == False) { 
			die("Probleme d'exécution de la requête SQL");
 		} else {
 			$message = "Modifications réussies !";		
 		}
		return $message;
	}

	public function getPseudo(String $email) : String {
		$conn = $this -> connectDB();
		$sql = "SELECT pseudo_user FROM User WHERE email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) {
			die("Probleme d'exécution de la requête SQL");
		} else {
			$pseudo = $result -> fetch();
			if ($pseudo == False) {
				return False;
			} else {
				return $pseudo[0];
			}
		}	
	}

}