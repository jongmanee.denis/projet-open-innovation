<?php


class PostManager extends Manager {
	
	public function listLast10Posts($email) {
		$conn = $this -> connectDB();
		$sql = 'SELECT date_post, text_post, lastName_user, firstName_user FROM Post INNER JOIN User ON Post.email_user=User.email_user WHERE User.email_user=? UNION SELECT date_post, text_post, u1.lastName_user, u1.firstName_user FROM Post INNER JOIN User u1 ON Post.email_user=u1.email_user INNER JOIN Friend ON u1.email_user=Friend.email_friend INNER JOIN User u2 ON Friend.email_user=u2.email_user WHERE u2.email_user=? ORDER BY date_post DESC LIMIT 10;';
		$result = $conn -> prepare($sql);
		$result -> execute(array($email,$email));
		if ($result == FALSE) { 
			die("Probleme d'exécution de la requête SQL"); 
		} else {
			return $result;
		} 	 
	}

	public function listUserLast10Posts($email) {
		$conn = $this -> connectDB();
		$sql = 'SELECT date_post, text_post FROM Post INNER JOIN User ON Post.email_user=User.email_user WHERE User.email_user=? ORDER BY date_post DESC LIMIT 10;';
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == FALSE) { 
			die("Probleme d'exécution de la requête SQL"); 
		} else {
			$posts = $result->fetchAll(PDO::FETCH_CLASS, "Post");
			return $posts;
		} 
	}

	public function addPost(Post $post) : String {
		$conn = $this -> connectDB();
		$sql = 'INSERT INTO Post (date_post, text_post, email_user) VALUES (?,?,?);';
		$result = $conn -> prepare($sql);
		$result -> execute(array($post->getDate_post(), $post->getText_post(), $post->getEmail_user()));
		if ($result == FALSE) { 
			die("Probleme d'exécution de la requête SQL"); 
		} else {
			return 'Votre poste a été publié !';
		}
	}
}