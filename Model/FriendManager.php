<?php

class FriendManager extends Manager {
	
	public function getFriends(String $email) {
		$conn = $this -> connectDB();
		$sql = "SELECT u2.lastName_user, u2.firstName_user, u2.email_user FROM User u1 INNER JOIN Friend ON u1.email_user=Friend.email_user INNER JOIN User u2 ON Friend.email_friend=u2.email_user WHERE Friend.email_user=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) { 
			die("Probleme d'exécution de la requête SQL"); 
		} else {
			$friends = $result->fetchAll(PDO::FETCH_CLASS, 'User');
			return $friends;
		} 	
	}

	public function getNoFriends(String $email) {
		$conn = $this -> connectDB();
		$sql = "SELECT lastName_user, firstName_user, email_user FROM User EXCEPT (SELECT u2.lastName_user, u2.firstName_user, u2.email_user FROM User u1 INNER JOIN Friend ON u1.email_user=Friend.email_user INNER JOIN User u2 ON Friend.email_friend=u2.email_user WHERE Friend.email_user=?);";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) { 
			die("Probleme d'exécution de la requête SQL"); 
		} else {
			$friends = $result->fetchAll(PDO::FETCH_CLASS, 'User');
			return $friends;
		} 	
	}

	public function addFriend($friend) {
		$conn = $this -> connectDB();
		$sql = "INSERT INTO Friend (email_user, email_friend) VALUES (?,?)";
		$result = $conn -> prepare($sql);
		$result -> execute(array($friend->getEmail_user(), $friend->getEmail_friend()));
		if ($result == False) { 
			die("Probleme d'exécution de la requête SQL"); 
		} else {
			$message = "Votre demande d'ajout a été effectué";
			return $message;
		} 
	}

}