<?php

class FollowManager extends Manager {
	
	public function getSportsUser(String $email) {
		$conn = $this->connectDB();
		$sql = 'SELECT * FROM Sport INNER JOIN Follow ON Sport.id_sport=Follow.num_sport WHERE Follow.email_user=?;';
		$result = $conn -> prepare($sql);
		$result -> execute(array($email));
		if ($result == False) {
			die('Problème de la requête sql');
		} else {
			$sports = $result->fetchAll(PDO::FETCH_CLASS, 'Sport');
			return $sports;
		}
	}

	public function followSport($email, $id) {
		$conn = $this->connectDB();
		$sql = "INSERT INTO Follow (email_user, num_sport) VALUES (?,?)";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email, $id));
		if ($result == False) {
			die('Problème de la requête sql');
		} else {
			$message = "Le sport a été ajouté à votre liste";
			return $message;
		}
	}	

	public function notFollowSport($email, $id) {
		$conn = $this->connectDB();
		$sql = "DELETE FROM Follow WHERE email_user=? AND num_sport=?;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email, $id));
		if ($result == False) {
			die('Problème de la requête sql');
		} else {
			$message = "Le sport a été supprimé à votre liste";
			return $message;
		}
	}
}