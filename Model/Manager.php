<?php

class Manager {

	protected const DB_HOST  = "localhost";
	protected const DB_USER = "root";
	protected const DB_PASSWORD = "";
	protected const DB_DATABASE = "poin";
	protected const DB_PORT = "3306";
	
	public function connectDB() {
		//$conn = mysqli_connect(self::DB_HOST, self::DB_USER, self::DB_PASSWORD, self::DB_DATABASE, self::DB_PORT); 
		//if (!$conn) {
		//	die('Erreur ' . mysqli_connect_errno() . ' : ' . mysqli_connect_error() . '<br>');
		//}
		//return $conn;
		try {
			$dsn = "mysql:host=". self::DB_HOST. ";dbname=" . self::DB_DATABASE . ";port=" . self::DB_PORT;
			$conn = new PDO($dsn, self::DB_USER, self::DB_PASSWORD); 
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			return $conn;
		} catch (Exception $e) { 
			die('Erreur : ' . $e->getMessage()); 
		} 
	}

	//public function disconnectDB($conn) {
	//	if (!mysqli_close($conn)) {
	//		die("echec de la deconnexion BD");
	//	}
	//}
}