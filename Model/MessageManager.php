<?php

class MessageManager extends Manager {
		
	public function getMessages($email_user, $email_friend) {
		$conn = $this -> connectDB();
		$sql = "SELECT firstName_user, lastName_user, text_message, date_message FROM User INNER JOIN Message ON User.email_user=Message.email_sender_user WHERE (email_sender_user=? AND email_receiver_user=?) OR (email_sender_user=? AND email_receiver_user=?) ORDER BY date_message;";
		$result = $conn -> prepare($sql);
		$result -> execute(array($email_user, $email_friend, $email_friend, $email_user));
		if ($result == False) {
			die("Probleme d'éxécution de la requete");
		} else {
			return $result;
		}
	}

	public function addMessage(Message $newmessage) {
		$conn = $this -> connectDB();
		$sql = "INSERT INTO Message (date_message, text_message, email_sender_user, email_receiver_user) VALUES (?,?,?,?);";
		$result = $conn -> prepare($sql);
		$result -> execute(array($newmessage->getDate_message(), $newmessage->getText_message(), $newmessage->getEmail_sender_user(), $newmessage->getEmail_receiver_user()));
		if ($result == False) {
			die("Probleme d'éxécution de la requete");
		} else {
			return 'votre message a été envoyé';
		}
	}
}