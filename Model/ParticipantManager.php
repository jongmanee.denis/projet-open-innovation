<?php

class ParticipantManager extends Manager {

	public function addParticipant(Participant $participant) {
		$conn = $this -> connectDB();
		$sql = "INSERT INTO Participant (num_event, email_user) VALUES (?,?)";
		$result = $conn -> prepare($sql);
		$result -> execute(array($participant->getNum_event(), $participant->getEmail_user()));
		if ($result == False) {
			die("Probleme d'éxécution de la requete");
		} else {
			return "Vous vous êtes inscrits à l'évènement";
		}
	}

	public function getParticipant(Participant $participant) {
		$conn = $this -> connectDB();
		$sql = "SELECT * FROM Participant WHERE num_event=? AND email_user=?";
		$result = $conn -> prepare($sql);
		$result -> execute(array($participant->getNum_event(), $participant->getEmail_user()));
		if ($result == False) {
			die("Probleme d'éxécution de la requete");
		} else {
			$participants = $result->fetchAll(PDO::FETCH_CLASS, 'Participant');
			return $participants;
		}
	}

}