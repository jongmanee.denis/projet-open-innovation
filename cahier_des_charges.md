Contexte

Une application de sport qui se distingue des autres applications déjà existantes avec des idées innovantes. 

L’application doit avoir un réseau social intégré pour permettre aux utilisateurs d’interagir entre eux et de faciliter l’exercice à plusieurs. 

L’application doit motiver les utilisateurs à revenir faire de l’exercice et les pousser à améliorer leurs performances.     

Objectifs

-	Augmentation des performances 
-	Booster l’énergie de l’utilisateur 
-	Motivation Coaching
-	Simplicité des tâches afin de commencer son entraînement 
-	Interaction avec les autres utilisateurs 
-	Promotions sur le réseau social, afin de donner envie aux utilisateurs de communiquer. 
-	Analyse complète des courses 
-	Permettre aux utilisateurs de se retrouver et faire du sport ensemble

Fonctions

Accéder à plusieurs catégories de sport
Pouvoir accéder à son mur social et à celui des autres
Pouvoir communiquer avec d’autres personnes selon les sports 
Enregistrer ses parcours de running
Avoir accès à des conseils sur un sport selon son niveau
Pouvoir augmenter sa réputation selon ses activités (événements, challenges, expériences…) et voir celles des autres
Ajouter des contacts et poster des photos et vidéos sur le mur social
Pouvoir rejoindre des personnes qui sont présentes et disponibles pour un sport
L’utilisateur peut accéder à ses performances 
Pouvoir créer des événements après avoir atteint une certaine réputation
Voir le classement général d’un sport 

Moyens Techniques à disposition 

HTML CSS3 
Javascript
Mysql 
Phpmyadmin
PhP

Moyens de communication à disposition 
Discord
Google doc

Répartition des tâches :

Groupe 1 : BDD : modélisation, phpmyadmin, sql - Programmation - techniques : soit php, sql - javascript

Groupe 2 : Programmation - design : HTML - CSS - javascript



