<?php 

switch ((isset($_GET["do"]) ? $_GET['do'] : "home")) {

	case 'listsports':
		switch ((isset($_GET["action"]) ? $_GET['action'] : "list")) {
			case 'add':
				addSport($_SESSION['email'], $_GET['id']);
				break;

			case 'del':
				delSport($_SESSION['email'], $_GET['id']);
				break;
			
			default:
			echo 'test';
				listSports($_SESSION['email']);
				break;
		}
		break;

	case 'event':
		if (isset($_GET['id']) && !isset($_GET['action'])) {
			detailsEvent($_SESSION['email'], $_GET['id']);
		} else {
			switch ((isset($_GET["action"]) ? $_GET['action'] : "list")) {

				case 'eventsport':
					listEventSport($_SESSION['email'], $_GET['id']);
					break;
				
				case 'form':
					formEvent($_SESSION['email']);
					break;

				case 'add':
					addEvent($_POST);
					break;
				
				case 'participate':
					participateEvent($_POST);
					break;


				case 'listevent':
					listEvents($_SESSION['email'], $_GET['email']);
					break;

				default:
					myEvents($_SESSION['email']);
					break;

			}
		}
		break;

	case 'advice':
		break;
	
	default:
		sportHome($_SESSION['email']);
		break;
}