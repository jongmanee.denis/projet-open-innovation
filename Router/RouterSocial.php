<?php

switch ((isset($_GET["do"]) ? $_GET['do'] : "home")) {

	case 'friend':
		switch((isset($_GET['action']) ? $_GET['action'] : "listfriends")) {
			case 'message':
				messaging($_SESSION['email'], $_GET['email']);
				break;

			case 'add':
				addFriend($_POST);
				break;

			case 'newmessage':
				addMessage($_POST);
				break;

			default:
				getFriends($_SESSION['email']);
				break;
		}
		break;

	case 'post':
		if (isset($_GET['email']) && !isset($_GET['action'])) {
				postsUser($_GET['email']);
		} else {
			switch((isset($_GET['action']) ? $_GET['action'] : "myposts")) {

				case 'newpost':
					addPost($_POST);
					break;

				default:
					postsUser($_SESSION['email']);
					break;
			}
		}
		break;


	default:
		socialHome($_SESSION['email']);
		break;
}