<?php
session_start();

include('./Controller/Controller.php');
include('./Controller/ControllerSocial.php');
include('./Controller/ControllerSport.php');

include('./Model/Entities/User.php');
include('./Model/Entities/Sport.php');
include('./Model/Entities/Post.php');
include('./Model/Entities/Friend.php');
include('./Model/Entities/Message.php');
include('./Model/Entities/Event.php');
include('./Model/Entities/Participant.php');

include('./Model/Manager.php');
include('./Model/UserManager.php');
include('./Model/FriendManager.php');
include('./Model/EventManager.php');
include('./Model/PostManager.php');
include('./Model/FollowManager.php');
include('./Model/SportManager.php');
include('./Model/MessageManager.php');
include('./Model/ParticipantManager.php');


if (isset($_SESSION['email'])) {

	switch ((isset($_GET["app"]) ? $_GET['app'] : "home")) {
		
		case 'profile':
			if (isset($_GET['email']) && !isset($_GET['do'])) {
				profileConsult($_GET['email']);
			} else {

				switch((isset($_GET['do']) ? $_GET['do'] : "consult")) {

					case 'formprofile':
						profileForm($_SESSION['email']);
						break;

					case 'updateprofile':
						profileUpdate($_POST);
						break;
					
					default:
						profileConsult($_SESSION['email']);
						break;	
				}
			}
			break;

		case 'social':
			include('./Router/RouterSocial.php');
			break;
		
		case 'sport':
			include('./Router/RouterSport.php');
			break;

		case 'logout':
			logoutUser();
			break;
			
		default:
			home($_SESSION['email']);
			break;
	}

} else {

	if (isset($_POST['email_user'])) {
		if ($_POST['do']=='registration') {
			registrationUser($_POST);
		} elseif ($_POST['do']=='login') {
			loginUser($_POST);
		}
	} else {
		firstpage();
	}
}