		CREATE TABLE User(
			email_user VARCHAR(40) NOT NULL,
		   pseudo_user VARCHAR(20),
		   lastName_user TEXT NOT NULL,
		   firstName_user VARCHAR(50) NOT NULL,
		   password_user VARCHAR(50) NOT NULL,
		   gender_user VARCHAR(1) NOT NULL,
		   birthday_user DATE NOT NULL	,
		   PRIMARY KEY(email_user)
		);

		CREATE TABLE Sport(
		   id_sport INT AUTO_INCREMENT,
		   name_sport VARCHAR(50),
		   description_sport VARCHAR(50),
		   PRIMARY KEY(id_sport)
		);

		CREATE TABLE Level(
		   id_level INT AUTO_INCREMENT,
		   wording_level VARCHAR(50),
		   PRIMARY KEY(id_level)
		);

		CREATE TABLE Post(
		   date_post DATETIME,
		   email_user VARCHAR(40) ,
		   text_post TEXT,
		   PRIMARY KEY(date_post, email_user),
		   FOREIGN KEY(email_user) REFERENCES User(email_user)
		);

		CREATE TABLE Event(
		   id_event INT AUTO_INCREMENT,
		   title_event VARCHAR(50),
		   description_event VARCHAR(50),
		   date_event DATETIME,
		   duration_event TIME,
		   number_places INT,
		   num_sport INT NOT NULL,
		   email_user VARCHAR(40) NOT NULL,
		   PRIMARY KEY(id_event),
		   FOREIGN KEY(num_sport) REFERENCES Sport(id_sport),
		   FOREIGN KEY(email_user) REFERENCES User(email_user)
		);

		CREATE TABLE Message(
		   date_message DATETIME,
		   text_message TEXT,
		   email_sender_user VARCHAR(40) NOT NULL,
		   email_receiver_user VARCHAR(40) NOT NULL,
		   PRIMARY KEY(date_message),
		   FOREIGN KEY(email_sender_user) REFERENCES User(email_user),
		   FOREIGN KEY(email_receiver_user) REFERENCES User(email_user)
		);

		CREATE TABLE Advice(
		   id_advice INT AUTO_INCREMENT,
		   text_advice VARCHAR(50),
		   num_level INT NOT NULL,
		   num_sport INT NOT NULL,
		   PRIMARY KEY(id_advice),
		   FOREIGN KEY(num_level) REFERENCES Level(id_level),
		   FOREIGN KEY(num_sport) REFERENCES Sport(id_sport)
		);

		CREATE TABLE Participant(
		   email_user VARCHAR(40),
		   num_event INT,
		   PRIMARY KEY(email_user, num_event),
		   FOREIGN KEY(email_user) REFERENCES User(email_user),
		   FOREIGN KEY(num_event) REFERENCES Event(id_event)
		);

		CREATE TABLE Follow(
		   email_user VARCHAR(40),
		   num_sport int,
		   PRIMARY KEY(email_user, num_sport),
		   FOREIGN KEY(email_user) REFERENCES User(email_user),
		   FOREIGN KEY(num_sport) REFERENCES Sport(id_sport)
		);

		CREATE TABLE Friend(
		   email_user VARCHAR(40),
		   email_friend VARCHAR(40),
		   PRIMARY KEY(email_user,email_friend),
		   FOREIGN KEY (email_user) REFERENCES User(email_user),
		   FOREIGN KEY (email_friend) REFERENCES User(email_user)
		);
