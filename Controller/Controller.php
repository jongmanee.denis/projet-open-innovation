<?php

function firstpage() {
	headerHTML("Bienvenue");
	require('./View/firstpage.php');
	require('./View/static/footer.php');
}
 
function registrationUser($post) {
	$user = new User($post);
	$error = False;

	$manager = new UserManager();

	if (!filter_var($user->getEmail_user(), FILTER_VALIDATE_EMAIL)) {
		$error = True;
		$message = "Mauvais format du mail";
	} else {
		if (strlen($user->getEmail_user())>40) {
			$error = True;
			$message = "Le mail est trop long";
		} else {
			if (!$manager ->uniqueEmail($user)) {
				$error = True;
				$message = "Cette addresse mail est déjà utilisée";
			}
		}
	}

	if ($user->getPassword_user() != $user->getCpassword_user()) {
		$error = True;
		$message = "Les mots de passe ne sont pas identiques";
	} else {
		if (strlen($user->getPassword_user())>50) {
			$error = True;
			$message = "Le mot de passe est trop long";
		}
	}

	if (strlen($user->getFirstName_user())>50) {
		$error = True;
		$message = "Le prénom est trop long";
	}


	$date = explode("-", $user->getBirthday_user());
  	$age = (date("md", date("U", mktime(0, 0, 0, $date[2], $date[1], $date[0]))) > date("md") ? ((date("Y") - $date[0]) - 1) : (date("Y") - $date[0]));
	if ($age<15) {
		$error = True;
		$message = "L'âge minimal est de 15 ans"; 
	}

	if (strlen($user->getPseudo_user())>20) {
		$error = True;
		$message = "Le pseudo est trop long";
	}

	if (!$error) {
		$message = $manager -> registration($user);
	}

	headerHTML("Bienvenue");
	require('./View/firstpage.php');
	require('./View/static/footer.php');
}

function loginUser($post) {
	$user = new User($post);
	$manager = new UserManager();
	$email = $manager -> login($user);
	if ($email) {
		$_SESSION['email'] = $email;
		header('Location:./index.php');
	} else {
		$message = "Identifications erronées";
		headerHTML("Bienvenue");
		require('./View/firstpage.php');
		require('./View/static/footer.php');
	}
}

function home($email) {
	$manager = new UserManager();
	$firstName = $manager -> getFirstName($email);
	$pseudo = $manager -> getPseudo($email);
	headerHTML("Accueil");
	require('./View/home.php');
	require('./View/static/footer.php');

}

function logoutUser() {
    session_destroy();
    unset($_SESSION);
	header('Location:./index.php');
}

function headerHTML(String $title) {
	include('./View/static/header.php');
}

function profileConsult($email) {

	$managerUser = new UserManager();
	$user = $managerUser -> getUser($email);

	$managerFollow = new FollowManager();
	$sports = $managerFollow -> getSportsUser($email);
	
	if ($email==$_SESSION['email']) {
		$title="Mon profil";
	} else {
		$title="Profil de " . $user->getFirstName_user() . " " . $user->getLastName_user();
	}

	require('./View/static/header.php');
	require('./View/bar/headerSocial.php');
	if ($email!=$_SESSION['email']) {
		require('./View/bar/user-bar.php');
	}
	require('./View/profile.php');
	
	require('./View/static/footer.php');
}

function profileForm($email) {

	$managerUser = new UserManager();
	$user = $managerUser -> getUser($email);

	$title="Modification du profil";

	require('./View/static/header.php');
	require('./View/bar/headerSocial.php');

	

	require('./View/updateprofile.php');

	require('./View/static/footer.php');
}

function profileUpdate($post) {
	$user = new User($post);
	$error = False;

	$manager = new UserManager();

	if (strlen($user->getFirstName_user())>50) {
		$error = True;
		$message = "Le prénom est trop long";
	}


	$date = explode("-", $user->getBirthday_user());
  	$age = (date("md", date("U", mktime(0, 0, 0, $date[2], $date[1], $date[0]))) > date("md") ? ((date("Y") - $date[0]) - 1) : (date("Y") - $date[0]));
	if ($age<15) {
		$error = True;
		$message = "L'âge minimal est de 15 ans"; 
	}

	if (strlen($user->getPseudo_user())>20) {
		$error = True;
		$message = "Le pseudo est trop long";
	}

	if (!$error) {
		$message = $manager -> updateProfile($user);
	}
	header('Location:./index.php?app=profile&do=formprofile&message=' . $message);

}