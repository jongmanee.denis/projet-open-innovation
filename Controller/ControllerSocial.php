<?php

#Before contain
function htmlSocialFirstPart(String $title, $email) {
	
	require('./View/static/header.php');
	
	require('./View/bar/headerSocial.php');

	#left-bar
	$managerFriend = new FriendManager();
	$friends = $managerFriend -> getFriends($email);
	require('./View/bar/leftbarSocial.php');
}

#After contain
function htmlSocialLastPart($email) {

	#right-bar
	$managerEvent = new EventManager();
	$events = $managerEvent -> listEventParticip($email);
 
	require('./View/bar/rightbarSocial.php');

	require('./View/static/footer.php');
}


###  Les différentes pages du site partie sociale  ###

function socialHome($email) {

	htmlSocialFirstPart("Accueil Réseau Social", $email);

	$manager = new PostManager();
	$posts = $manager -> listLast10Posts($email);
	require('./View/social/homeSocial.php');

	htmlSocialLastPart($email);
}

function addPost($post) {
	
	$newpost = new Post($post);

	$manager = new PostManager();
	$message = $manager -> addPost($newpost);

	header('Location:./index.php?app=social&do=post&message=' . $message);

}

function getFriends($email) {

	htmlSocialFirstPart("Liste d'amis", $email);

	$manager = new FriendManager();
	$friends = $manager -> getFriends($email);
	$users = $manager -> getNoFriends($email);

	require('./View/social/listFriends.php');
	
	htmlSocialLastPart($email);
}

function messaging($email_user, $email_friend) {

	htmlSocialFirstPart("Messagerie", $email_user);

	$manager = new MessageManager();
	$messages = $manager -> getMessages($email_user, $email_friend);
	
	require('./View/social/listMessages.php');
	
	htmlSocialLastPart($email_user);
}

function addMessage($post) {
	
	$newmessage = new Message($post);
	$manager = new MessageManager();
	$message = $manager -> addMessage($newmessage);

	header('Location:./index.php?app=social&do=friend&action=message&email=' . $newmessage->getEmail_receiver_user() . '&message=' . $message);
}

function postsUser($email) {
	
	$managerUser = new UserManager();
	$user = $managerUser -> getUser($email);

	$managerPost = new PostManager();
	$posts = $managerPost -> listUserLast10Posts($email);

	if ($email==$_SESSION['email']) {
		htmlSocialFirstPart("Mes postes ", $_SESSION['email']);
		require('./View/social/listPosts.php');
	} else {
	require('./View/static/header.php');
	
	require('./View/bar/headerSocial.php');
	require('./View/bar/user-bar.php');
	#left-bar
	$managerFriend = new FriendManager();
	$friends = $managerFriend -> getFriends($email);
	require('./View/bar/leftbarSocial.php');
		require('./View/social/listPosts.php');
		
	}
	htmlSocialLastPart($_SESSION['email']);
}
