<?php 

function htmlSportFirstPart(String $title, $email) {
	$managerEvent = new EventManager();
	$events = $managerEvent -> listFriendsEvents($email);
	
	require('./View/static/header.php');

	require('./View/bar/headerSport.php');
	require('./View/bar/leftbarSport.php');	
}


function htmlSportLastPart($email) {
	$managerFriend = new FriendManager();
	$friends = $managerFriend -> getFriends($email);
	require('./View/bar/rightbarSport.php');
	require('./View/static/footer.php');
}

function sportHome($email) {
	
	$managerEvent = new EventManager();
	$futurEvents = $managerEvent ->listEventParticip($email);

	$someEvents = $managerEvent -> getSomeEvents($email);

	$managerSport = new SportManager();
	htmlSportFirstPart("Accueil Sport",$email);

	require('./View/sport/homeSport.php');

	htmlSportLastPart($email);

}

function listSports($email) {
	
	$managerFollow = new FollowManager();
	$sportsUser = $managerFollow -> getSportsUser($email);
	
	$managerSport = new SportManager();
	$sports = $managerSport -> getOthersSports($email);

	htmlSportFirstPart("Liste des sports", $email);

	require('./View/sport/listSport.php');

	htmlSportLastPart($email);
}

function addSport($email, $id) {
	$managerFollow = new FollowManager();
	$message = $managerFollow -> followSport($email,$id);
	header('Location:./index.php?app=sport&do=listsports&message=' . $message);

}

function delSport($email, $id) {
	$managerFollow = new FollowManager();
	$message = $managerFollow -> notFollowSport($email,$id);
	header('Location:./index.php?app=sport&do=listsports&message=' . $message);
}

function myEvents($email) {
	
	$managerEvent = new EventManager();
	
	$createdEvents = $managerEvent -> getCreatedEventsUser($email);
	$participEvents = $managerEvent -> listAllEventParticip($email);

	$managerSport = new SportManager();

	htmlSportFirstPart("Liste de mes évenements",$email);

	require('./View/sport/listEventUser.php');

	htmlSportLastPart($email);
}

function formEvent($email) {
	htmlSportFirstPart("Evenement",$email);

	$manager = new SportManager();
	$sports = $manager -> getSports();	
	require('./View/sport/formEvent.php');

	htmlSportLastPart($email);
}

function addEvent($post) {
	$event = new Event($post);
	$manager = new EventManager();
	$message = $manager -> addEvent($event);
	header('Location:./index.php?app=sport&do=event&message=' . $message);
}

function listEventSport($email, $id) {

	$manager = new EventManager();
	$events = $manager->getEventByIdSport($id);
	$managerSport = new SportManager();
	htmlSportFirstPart("Liste des évenements",$email);

	require('./View/sport/listEvent.php');

	htmlSportLastPart($email);
}

function detailsEvent($email, $id) {
	$manager = new EventManager();
	$event = $manager->getEvent($id);
	$managerSport = new SportManager();
	$managerUser = new UserManager();

	htmlSportFirstPart("Détails de l'évènement",$email);

	require('./View/sport/detailsEvent.php');

	htmlSportLastPart($email);
}

function participateEvent($post) {
	$participant = new Participant($post);
	$manager = new ParticipantManager();

	$participants = $manager -> getParticipant($participant);

	if (count($participants)!=0) {
		$message = 'Vous participez déjà à cet évènement';
	} else {
		$message = $manager -> addParticipant($participant);
	}
	header('Location:./index.php?app=sport&do=event&id='. $participant->getNum_event() . '&message=' . $message);

}

function listEvents($email_user, $email) {

	
	$manager = new EventManager();
	$createdEvents = $manager -> getCreatedEventsUser($email);
	$participEvents = $manager -> listAllEventParticip($email);
	$managerEvent = new EventManager();
	$events = $managerEvent -> listFriendsEvents($email);
	
	require('./View/static/header.php');

	require('./View/bar/headerSport.php');
require('./View/bar/user-bar.php');
	require('./View/bar/leftbarSport.php');	
	
	require('./View/sport/listEventUser.php');

	htmlSportLastPart($email_user);
}