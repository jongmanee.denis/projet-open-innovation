<div class="col-6">
	<div class="message"><?php if (isset($_GET['message'])) echo $_GET['message']; ?> </div>
	<div style="margin-bottom:30px;">
		<span>Vous avez actuellement <?php echo count($friends); ?> amis dans votre liste</span>
	</div>
	<?php foreach ($friends as $friend) { ?>
		<div class="row" style="margin-bottom: 10px">
			<div class="col-6">
				<?php echo $friend->getLastName_user(); ?> <?php echo $friend->getFirstName_user(); ?>
			</div>
			<div class="col-6">
			<a href="./index.php?app=profile&email=<?php echo $friend->getEmail_user(); ?>"><button class="btn btn-secondary">Consulter le profil</button></a>
			<a href="./index.php?app=social&do=friend&action=message&email=<?php echo $friend->getEmail_user(); ?>"><button class="btn btn-primary">Message</button></a>
			</div>	
		</div>
	<?php } ?>

	<div>
		<h2>Liste des utilisateurs</h2>
		<?php foreach ($users as $user) { ?>
		<div class="row" style="margin-bottom: 10px">
			<div class="col-6">
				<?php echo $user->getLastName_user(); ?> <?php echo $user->getFirstName_user(); ?>
			</div>
			<div class="col-6">
				
					<a href="./index.php?app=profile&email=<?php echo $user->getEmail_user(); ?>"><button class="btn btn-secondary">Consulter le profil</button></a>
			<a href="./index.php?app=social&do=friend&action=message&email=<?php echo $user->getEmail_user(); ?>"><button class="btn btn-primary">Message</button></a>
								<form action="./index.php?app=social&do=friend&action=add" method="POST">
					<input type="hidden" name="email_friend" value="<?php echo $user->getEmail_user(); ?>">
					<input type="hidden" name="email_user" value="<?php echo $_SESSION['email']; ?>">
					<input type="submit" value="Ajouter"class="btn btn-success">

				</form>

			</div>

		</div>
		<?php } ?>
	</div>
</div>
