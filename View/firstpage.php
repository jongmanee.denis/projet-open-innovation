<div class="sidenav">
    <div class="login-main-text" style="text-align: center">
		<img src="./Public/img/logo.png" alt="logo" style="width:20%; "/>
		<p>Le réseau social de sport</p>
	</div>
</div>
<div class="container">
        <div class="login-form" style="text-align: center">
			<button id="login" class="btn btn-primary gras" onclick="hide('formlogin');gras('login')">Connexion</button>
			<button id="registration" class="btn btn-primary gras" onclick="hide('formregistration');gras('registration')">Inscription</button>
		<div><span style="color:red;"><?php if (isset($message)) { echo $message; } ?> </span></div>
		<div class="hide" id="formlogin" style="display:none">
			<form action="./index.php" method="POST">
				<label for="email_login">Adresse Mail</label></br>
				<input class="form-control" type="text" class="form-control" name="email_user" id="email_login" required/></br></br>
				<label for="password_login">Mot de passe</label></br>
				<input class="form-control" type="password" name="password_user" id="password_login" required/></br></br>
				<input type="hidden" name="do" value="login">
				<input type="submit" class="btn btn-success" value="Se connecter"/>
			</form>
		</div>

	
	<div class="hide" id="formregistration" style="display:none">
		<form action="./index.php" method="POST">
		<div class="row">
			
				 <div class="col-6">
					<div class="form-group">
						<label for="lastName_user" class="label">Nom</label></br>
						<input class="form-control" type="text" name="lastName_user" id="lastName_user" required/></br></br>
					</div>
					<div class="form-group">
						<label for="firstName_user">Prénom</label></br>
						<input class="form-control" type="text" name="firstName_user" id="firstName_user" required/></br></br>
					</div>
					<div class="form-group">
						<label for="birthday_user">Date de naissance</label></br>
						<input class="form-control" type="date" name="birthday_user" id="birthday_user" required/></br>
					</div>
					<div class="form-group">
						<p>Sexe</p>
						<input type="radio" name="gender_user" id="h" value="h"/>
						<label for="h">Homme</label>
						<input type="radio" name="gender_user" id="f" value="f"/>
						<label for="f">Femme</label></br></br>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="pseudo_user">Pseudo</label></br>
						<input class="form-control" type="text" name="pseudo_user" id="pseudo_user"/></br></br>
					</div>
					<div class="form-group">
						<label for="email_user">Adresse Mail</label></br>
						<input class="form-control" type="text" name="email_user" id="email_user" required/></br></br>
					</div>
					<div class="form-group">
						<label for="password">Mot de passe</label></br>
						<input class="form-control" type="password" name="password_user" id="password" required/></br></br>
					</div>
					<div class="form-group">
						<label for="cpassword">Confirmation du mot de passe</label></br>
						<input class="form-control" type="password" name="cpassword_user" id="cpassword" required/></br></br>
					</div>
					<input type="hidden" name="do" value="registration"/>
					
				</div>
			
			</div>
			<input type="submit" class="btn btn-success" value="S'inscrire"/>
		</form>
	</div>
</div>
