<div class="col-6">
	<div>
		<h2>Vos évènements à venir</h2>
		<table class="table">
			<tr>
				<td>Sport</td>
				<td>Date</td>
				<td>Nombre de personnes</td>
				<td>Evenement</td>
				<td></td>
			</tr>
			<?php foreach ($futurEvents as $event) { ?>
				<tr>
					<td><?php echo $managerSport->getName_sportbyId($event->getNum_sport()); ?></td>
					<td><?php echo $event->getDate_event(); ?></td>
					<td><?php echo $event->getNumber_places(); ?></td>
					<td><a href="./index.php?app=sport&do=event&id=<?php echo $event->getId_event(); ?>"><button>Voir les détails</button></a></td>
				</tr>
			<?php } ?>
		</table>
	</div>
	<div>
		<h2>Des évènements qui pourraient vous interesser</h2>
		<table class="table">
			<tr>
				<td>Sport</td>
				<td>Date</td>
				<td>Nombre de personnes</td>
				<td>Evenement</td>
				<td></td>
			</tr>
			<?php foreach ($someEvents as $event) { ?>
				<tr>
					<td><?php echo $managerSport->getName_sportbyId($event->getNum_sport()); ?></td>
					<td><?php echo $event->getDate_event(); ?></td>
					<td><?php echo $event->getNumber_places(); ?></td>
					<td><a href="./index.php?app=sport&do=event&id=<?php echo $event->getId_event(); ?>"><button>Voir les détails</button></a></td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>