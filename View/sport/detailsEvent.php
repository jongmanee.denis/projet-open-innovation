<div class="col-6">
	<h2>Détails de l'évènement</h2>
	<ul>
		<li>Intitulé : <?php echo $event->getTitle_event(); ?></li>
		<li>Date de l'évènement : <?php echo $event->getDate_event(); ?></li>
		<li>Description : <?php echo $event->getDescription_event(); ?></li>
		<li>Durée de l'évènement : <?php echo $event->getDuration_event(); ?></li>
		<li>Nombre de places maximum : <?php echo $event->getNumber_places(); ?></li>
		<li>Concerne le sport <?php echo $managerSport->getName_sportbyId($event->getNum_sport()); ?></li>
		<li>Evenement créé par <?php echo $managerUser->getLastName($event->getEmail_user()) . " " . $managerUser->getfirstName($event->getEmail_user()) ; ?></li>	
	</ul>
	<?php if ($_SESSION['email']!=$event->getEmail_user()) { ?>
		<form action="./index.php?app=sport&do=event&action=participate" method="POST">
			<input type="hidden" name="num_event" value="<?php echo $event->getId_event(); ?>">
			<input type="hidden" name="email_user" value="<?php echo $_SESSION['email']; ?>">
			<input type="submit" class="btn btn-secondary" value="Participer à cet évènement">
		</form>	
	<?php	} ?>
</div>