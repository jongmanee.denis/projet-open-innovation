<div class="col-6">
	<?php if (isset($_GET['message'] )) {
		echo '<p>' . $_GET['message'] . '</p>';
	} 
	if (!isset($_GET['email'])) { ?>
		<a href="./index.php?app=sport&do=event&action=form"><button class="btn btn-primary">Créer un évènement</button></a>
	<?php } ?>	
	<div>
		<h3>Evenements créés</h3>
		<?php if (count($createdEvents)==0) {
			echo "Aucun évènement n'a été créé";
		} else { ?>

		<table class="table">
			<?php foreach ($createdEvents as $event) { ?>
				<tr>
					<td><?php echo $event->getTitle_event(); ?></td>
					<td><?php echo $event->getDate_event(); ?></td>

					<td><?php echo $managerSport->getName_sportbyId($event->getNum_sport()); ?></td>
					<td><a href="./index.php?app=sport&do=event&id=<?php echo $event->getId_event(); ?>"><button class="btn btn-primary">Voir détails</button></a></td>
				</tr>
			<?php } ?>
		</table>
	<?php } ?>
	</div>
		<div>
		<h3>Evenements participés</h3>
				<?php if (count($participEvents)==0) {
			echo "Aucun évènement n'a été participé";
		} else { ?>
		<table class="table">
			<?php foreach ($participEvents as $event) { ?>
				<tr>
					<td><?php echo $event->getTitle_event(); ?></td>
					<td><?php echo $event->getDate_event(); ?></td>

					<td><?php echo $managerSport->getName_sportbyId($event->getNum_sport()); ?></td>
				
					<td><a href="./index.php?app=sport&do=event&id=<?php echo $event->getId_event(); ?>"><button class="btn btn-primary">Voir détails</button></a></td>
				</tr>
			<?php } ?>
		</table>
		<?php } ?>
	</div>
</div>