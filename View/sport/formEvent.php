<div class="col-6">

	<h2>Créer un évènement</h2>
	<form action="./index.php?app=sport&do=event&action=add" method="POST">
		<label for="title">Titre</label>
		<input class="form-control" type="text" name="title_event" id="title" required/>
		<label for="description">Description</label>
		<input class="form-control" type="text" name="description_event" id="description" required/>
		<label for="date">Date de l'evenement</li></label>
		<input class="form-control" type="datetime-local" name="date_event" id="date" required/>
		<label for="duration">Durée</label>
		<input  class="form-control"type="time" name="duration_event" id="duration" required/>
		<label for="places">Nombre de personnes</label>
		<input class="form-control" type="number" name="number_places" id="places" min="0" required/>
		<label for="sport">Sport</label>
		<select class="form-control" name="num_sport" id="sport">
			<?php foreach ($sports as $sport) { ?>
				<option value="<?php echo $sport->getId_sport(); ?>"><?php echo $sport->getName_sport(); ?></option>
			<?php } ?>
		</select>
		<input type="hidden" name="email_user" value="<?php echo $_SESSION['email']; ?>">
		<input type="submit" class="btn btn-primary" value="Créer">
	</form>
</div>