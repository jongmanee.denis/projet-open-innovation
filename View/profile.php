<div style="text-align: center">
	<div>
		<h2>Informations</h2>
		<ul class="list-group">
			<li class="list-group-item"><?php echo $user->getFirstName_user() . " " . $user->getLastname_user(); ?></li>
			<li class="list-group-item">Pseudo : <?php echo ($user->getPseudo_user()!="" ? $user->getPseudo_user() : 'Pas de pseudo');  ?></li>
			<li class="list-group-item">Date de naissace : <?php echo $user->getBirthday_user(); ?></li>
			<li class="list-group-item">Sexe : <?php echo (($user->getGender_user()=="m") ? 'Homme' : 'Femme'); ?></li>

			<?php if (!isset($_GET['email'])) { ?>
				<li class="list-group-item">Adresse Mail : <?php echo $user->getEmail_user(); ?></li>
						</ul>
				<a href="./index.php?app=profile&do=formprofile"><button class="btn btn-primary">Modifier mes informations</button></a>
			<?php } ?>

	</div>	
	<div>
		<h2>Préférences</h2>
		<table class="table">
			<tr>
				<td colspan="2">Liste des sports suivis :</td>
			</tr>
			<?php foreach ($sports as $sport) { ?>
			<tr>
				<td><?php echo $sport->getName_sport(); ?></td>
				<td><?php echo $sport->getDescription_sport(); ?></td>
			</tr>
			<?php } ?>
		</table>
		<?php if (!isset($_GET['email'])) { ?>
			<p><a href="./index.php?app=sport&do=listsports"><button class="btn btn-primary">Modifier sa liste des sports suivis</button></p>
		<?php } ?>
	</div>
</div>