<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<div class="container">
		  	<div class="row justify-content-start">
		     	<div class="col-3">	
					<img src="./Public/img/logo.png" style="width:30%">
				</div>
				<div class="col-9">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active"><a class="nav-link" href="./index.php?app=social">Les postes récents</a></li>
						<li class="nav-item active"><a class="nav-link" href="./index.php?app=social&do=friend">Rechercher un utilisateur</a></li>

						<li class="nav-item active"><a class="nav-link"  href="./index.php?app=sport&do=sport">Les sports</a></li>
						<li class="nav-item active"><a class="nav-link"  href="./index.php?app=sport&do=event">Evènements</a></li>
						<?php include('./View/bar/myprofileButton.php'); ?>		
					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>