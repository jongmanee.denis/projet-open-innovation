<div class="container">
  <div class="row justify-content-start">

    <div class="col-3">

	<ul class="nav flex-column">
	  	<li class="nav-item"> <a class="nav-link" href="./index.php?app=social&do=post">Mes postes</a></li>
		<li class="nav-item"><a class="nav-link" href="./index.php?app=social&do=photo">Photos</a></li>
		<li class="nav-item"><a class="nav-link" href="./index.php?app=social&do=video">Vidéos</a></li>
	</ul>
	<ul class="nav flex-column">Liste d'amis
		<?php foreach ($friends as $friend) { ?>
			<li class="nav-item"><a class="nav-link" href="./index.php?app=profile&email=<?php echo $friend->getEmail_user(); ?>"><?php echo $friend->getLastName_user() . " " . $friend->getFirstName_user(); ?></a></li>
		<?php } ?>
	</ul>

	</div>
