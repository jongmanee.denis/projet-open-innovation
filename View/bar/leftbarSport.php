<div class="container">
  <div class="row justify-content-start">
  	
    <div class="col-3">

	<ul class="nav flex-column">
			<li class="nav-item"><a class="nav-link" href="./index.php?app=sport&do=sport">Les sports</a></li>
			<li class="nav-item"><a class="nav-link" href="./index.php?app=sport&do=event">Les évènements</a></li>
		</ul>
		<ul class="nav flex-column"> Evènements créés par mes amis
			<?php while ($ligne = $events -> fetch()) { ?>
			<li><a class="nav-link" href="./index.php?app=sport&do=event&id=<?php echo $ligne['id_event']; ?>"><?php echo $ligne['title_event'] . " organisé par " . $ligne['firstName_user'] . " " . $ligne['lastName_user']; ?></a></li>
			<?php } ?>
		</ul>
	</nav>
</div>